package com.taxi.manager.model;

import com.taxi.manager.exception.AddressNotValidException;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Address {

    private String city;
    private String street;
    private String houseNumber;

    public boolean validate() {
        if (this.city == null) throw new AddressNotValidException("City");
        if (this.street == null) throw new AddressNotValidException("Street");
        if (this.houseNumber == null) throw new AddressNotValidException("House number");

        return true;
    }
}
