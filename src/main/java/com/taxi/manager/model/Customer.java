package com.taxi.manager.model;

import com.taxi.manager.controller.dto.CustomerRequestDTO;
import com.taxi.manager.converter.AddressConverter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(nullable = false)
    private String name;
    @Convert(converter = AddressConverter.class)
    private Address homeAddress;
    @OneToMany(mappedBy = "customer")
    private List<Order> order;
    @OneToMany(mappedBy = "customer")
    private List<Rating> rating;

    public static Customer from(CustomerRequestDTO createCustomerRequestDTO) {
        Customer customer = new Customer();
        customer.setName(createCustomerRequestDTO.getName());
        customer.setHomeAddress(createCustomerRequestDTO.getHomeAddress());

        return customer;
    }

    public void update(CustomerRequestDTO customerRequestDTO) {
        this.name = customerRequestDTO.getName();
        this.homeAddress = customerRequestDTO.getHomeAddress();
    }
}
