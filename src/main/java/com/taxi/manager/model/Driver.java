package com.taxi.manager.model;

import com.taxi.manager.controller.dto.CreateDriverRequestDTO;
import com.taxi.manager.controller.dto.UpdateDriverRequestDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Driver {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(nullable = false)
    private String name;
    private String city;
    private String languages;
    private String drivingLicense;
    @OneToOne(mappedBy = "driver", cascade = CascadeType.ALL,
            fetch = FetchType.LAZY, optional = false)
    private Car car;
    @OneToMany(mappedBy="driver")
    private List<Order> order;
    @OneToMany(mappedBy="driver")
    private List<Rating> rating;

    public static Driver from(CreateDriverRequestDTO createDriverRequestDTO) {
        Driver driver = new Driver();
        driver.setName(createDriverRequestDTO.getName());
        driver.setCity(createDriverRequestDTO.getCity());
        driver.setLanguages(createDriverRequestDTO.getLanguages());
        driver.setDrivingLicense(createDriverRequestDTO.getDrivingLicense());

        return driver;
    }

    public void update(UpdateDriverRequestDTO updateDriverRequestDTO) {
        this.name = updateDriverRequestDTO.getName();
        this.city = updateDriverRequestDTO.getCity();
        this.languages = updateDriverRequestDTO.getLanguages();
    }
}
