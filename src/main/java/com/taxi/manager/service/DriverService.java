package com.taxi.manager.service;

import com.taxi.manager.controller.dto.CreateDriverRequestDTO;
import com.taxi.manager.controller.dto.DriverDTO;
import com.taxi.manager.controller.dto.UpdateDriverRequestDTO;
import com.taxi.manager.exception.DriverNotFoundException;
import com.taxi.manager.model.Driver;
import com.taxi.manager.repository.DriverRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class DriverService {

    private final DriverRepository driverRepository;

    public Driver save(CreateDriverRequestDTO createDriverRequestDTO) {
        Driver driver = Driver.from(createDriverRequestDTO);
        driverRepository.save(driver);
        return driver;
    }

    public Optional<Driver> findById(Long id) {
        return driverRepository.findById(id);
    }

    public DriverDTO update(UpdateDriverRequestDTO updateDriverRequestDTO, Long id) {
        Optional<Driver> optionalDriver = findById(id);

        if (!optionalDriver.isPresent()) {
            throw new DriverNotFoundException(id);
        }

        Driver driver = optionalDriver.get();
        driver.update(updateDriverRequestDTO);
        driverRepository.save(driver);
        return DriverDTO.from(driver);
    }
}
