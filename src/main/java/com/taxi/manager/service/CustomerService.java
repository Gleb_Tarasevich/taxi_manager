package com.taxi.manager.service;


import com.taxi.manager.controller.dto.CustomerRequestDTO;
import com.taxi.manager.exception.CustomerNotFoundException;
import com.taxi.manager.model.Customer;
import com.taxi.manager.repository.CustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CustomerService {

    private final CustomerRepository customerRepository;

    public Optional<Customer> findById(Long id) {
        return customerRepository.findById(id);
    }

    public Customer save(CustomerRequestDTO customerRequestDTO) {
        Customer customer = Customer.from(customerRequestDTO);
        return customerRepository.save(customer);
    }

    public Customer update(CustomerRequestDTO customerRequestDTO, Long id) {
        Optional<Customer> customerOptional = findById(id);

        if (!customerOptional.isPresent()) {
            throw new CustomerNotFoundException(id);
        }

        Customer customer = customerOptional.get();
        customer.update(customerRequestDTO);
        customerRepository.save(customer);
        return customer;
    }
}
