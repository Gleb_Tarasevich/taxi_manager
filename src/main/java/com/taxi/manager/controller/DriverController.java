package com.taxi.manager.controller;

import com.taxi.manager.controller.dto.CreateDriverRequestDTO;
import com.taxi.manager.controller.dto.DriverDTO;
import com.taxi.manager.controller.dto.UpdateDriverRequestDTO;
import com.taxi.manager.model.Driver;
import com.taxi.manager.service.DriverService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/driver")
public class DriverController {

    private final DriverService driverService;

    @GetMapping("/{id}")
    public ResponseEntity<DriverDTO> getById(@PathVariable Long id) {
       return driverService.findById(id)
               .map(DriverDTO::from)
               .map(ResponseEntity::ok)
               .orElseGet(ResponseEntity.notFound()::build);
    }

    @PostMapping
    public ResponseEntity<DriverDTO> save(@RequestBody CreateDriverRequestDTO request) {
        request.validate();

        Driver driver = driverService.save(request);

        DriverDTO driverDTO = DriverDTO.from(driver);
        return ResponseEntity.ok(driverDTO);
    }

    @PutMapping("/{id}")
    public ResponseEntity<DriverDTO> update(@RequestBody UpdateDriverRequestDTO request, @PathVariable Long id) {
        request.validate();

        DriverDTO driverDto = driverService.update(request, id);
        return ResponseEntity.ok(driverDto);
    }
}
