package com.taxi.manager.controller;

import com.taxi.manager.controller.dto.CustomerDTO;
import com.taxi.manager.controller.dto.CustomerRequestDTO;
import com.taxi.manager.model.Customer;
import com.taxi.manager.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/customer")
public class CustomerController {

    private final CustomerService customerService;

    @GetMapping("/{id}")
    public ResponseEntity<CustomerDTO> findById(@PathVariable Long id) {
        return customerService.findById(id)
                .map(CustomerDTO::from)
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.notFound()::build);
    }

    @PostMapping
    public ResponseEntity<CustomerDTO> save(@RequestBody CustomerRequestDTO request) {
        request.validate();

        Customer customer = customerService.save(request);
        CustomerDTO customerDto = CustomerDTO.from(customer);
        return ResponseEntity.ok(customerDto);
    }

    @PutMapping("/{id}")
    public ResponseEntity<CustomerDTO> update(@RequestBody CustomerRequestDTO request, @PathVariable Long id) {
        request.validate();

        Customer customer = customerService.update(request, id);
        CustomerDTO customerDto = CustomerDTO.from(customer);
        return ResponseEntity.ok(customerDto);
    }
}