package com.taxi.manager.controller.exception;

import com.taxi.manager.controller.dto.ErrorResponseDTO;
import com.taxi.manager.exception.DriverNotFoundException;
import com.taxi.manager.exception.DriverNotValidException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class DriverHandlerException {

    @ExceptionHandler(DriverNotValidException.class)
    public ResponseEntity<ErrorResponseDTO> handle(DriverNotValidException e) {
        ErrorResponseDTO errorResponseDTO = new ErrorResponseDTO(e.getMessage());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponseDTO);
    }

    @ExceptionHandler(DriverNotFoundException.class)
    public ResponseEntity<ErrorResponseDTO> handle(DriverNotFoundException e) {
        ErrorResponseDTO errorResponseDTO = new ErrorResponseDTO(e.getMessage());
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(errorResponseDTO);
    }
}
