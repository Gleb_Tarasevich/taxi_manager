package com.taxi.manager.controller.exception;

import com.taxi.manager.controller.dto.ErrorResponseDTO;
import com.taxi.manager.exception.AddressNotValidException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class AddressExceptionHandler {

    @ExceptionHandler(AddressNotValidException.class)
    public ResponseEntity<ErrorResponseDTO> handle(AddressNotValidException e) {
        ErrorResponseDTO errorResponseDTO = new ErrorResponseDTO(e.getMessage());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponseDTO);
    }
}
