package com.taxi.manager.controller.dto;

import com.taxi.manager.exception.DriverNotValidException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CreateDriverRequestDTO {

    private String name;
    private String city;
    private String drivingLicense;
    private String languages;
    private CarDTO car;

    public boolean validate() {
        if (name == null) throw new DriverNotValidException("name");
        if (city == null) throw new DriverNotValidException("city");
        if (drivingLicense == null) throw new DriverNotValidException("driving license");
        if (car == null) throw new DriverNotValidException("car");

        return true;
    }
}
