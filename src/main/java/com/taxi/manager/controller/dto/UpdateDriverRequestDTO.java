package com.taxi.manager.controller.dto;

import com.taxi.manager.exception.DriverNotValidException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UpdateDriverRequestDTO {

    private String name;
    private String city;
    private String languages;
    private CarDTO car;

    public boolean validate() {
        if (name == null || name.length() < 4) throw new DriverNotValidException("name");
        if (city == null) throw new DriverNotValidException("city");
        if (languages == null) throw new DriverNotValidException("languages");
        if (car == null) throw new DriverNotValidException("car");

        return true;
    }
}
