package com.taxi.manager.controller.dto;

import lombok.Data;

@Data
public class UserDetailsDTO {

    private String password;
    private String email;
    private String phone;
}
