package com.taxi.manager.controller.dto;

import com.taxi.manager.exception.CustomerNotValidException;
import com.taxi.manager.model.Address;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CustomerRequestDTO {

    private String name;
    private Address homeAddress;

    public boolean validate() {
        if (name == null) throw new CustomerNotValidException("Name");
        homeAddress.validate();

        return true;
    }
}
