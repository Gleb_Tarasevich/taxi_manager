package com.taxi.manager.controller.dto;

import com.taxi.manager.model.Driver;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DriverDTO {

    private Long id;
    private String name;
    private String city;
    private String languages;
    private String drivingLicense;
    private CarDTO car;
    private OrderDTO order;
    private RatingDTO rating;

    public static DriverDTO from(Driver driver) {
        DriverDTO driverDto = new DriverDTO();
        driverDto.setId(driver.getId());
        driverDto.setName(driver.getName());
        driverDto.setCity(driver.getCity());
        driverDto.setLanguages(driver.getLanguages());
        driverDto.setDrivingLicense(driver.getDrivingLicense());
        driverDto.setCar(CarDTO.from(driver.getCar()));
        driverDto.setOrder(OrderDTO.from(driver.getOrder()));
        driverDto.setRating(RatingDTO.from(driver.getRating()));

        return driverDto;
    }
}
