package com.taxi.manager.controller.dto;

import com.taxi.manager.model.Address;
import com.taxi.manager.model.Customer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CustomerDTO {

    private Long id;
    private String name;
    private Address homeAddress;
    private OrderDTO order;
    private RatingDTO rating;

    public static CustomerDTO from(Customer customer) {
        CustomerDTO customerDto = new CustomerDTO();
        customerDto.setId(customer.getId());
        customerDto.setName(customer.getName());
        customerDto.setHomeAddress(customer.getHomeAddress());
        customerDto.setOrder(OrderDTO.from(customer.getOrder()));
        customerDto.setRating(RatingDTO.from(customer.getRating()));

        return customerDto;
    }
}
