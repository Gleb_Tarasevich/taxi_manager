package com.taxi.manager.exception;

public class AddressNotValidException extends RuntimeException {

    public AddressNotValidException(String message) {
        super("Not valid field: " + message);
    }
}
