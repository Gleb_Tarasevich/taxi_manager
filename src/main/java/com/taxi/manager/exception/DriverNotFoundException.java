package com.taxi.manager.exception;

public class DriverNotFoundException extends RuntimeException {

    public DriverNotFoundException(Long id) {
        super("Driver with id "  + id +  " was not found");
    }
}
