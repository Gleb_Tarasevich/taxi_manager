package com.taxi.manager.exception;

public class CustomerNotValidException extends RuntimeException {

    public CustomerNotValidException(String message) {
        super("Not valid field: " + message);
    }
}
