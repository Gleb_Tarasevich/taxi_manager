package com.taxi.manager.exception;

public class ConverterException extends RuntimeException {

    public ConverterException(String message) {
        super("Conversion error: " + message);
    }
}
