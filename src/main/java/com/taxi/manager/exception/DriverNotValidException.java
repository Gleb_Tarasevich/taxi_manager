package com.taxi.manager.exception;

public class DriverNotValidException extends RuntimeException {

    public DriverNotValidException(String message) {
        super("Not valid field: " + message);
    }
}
