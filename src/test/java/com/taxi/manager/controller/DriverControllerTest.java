package com.taxi.manager.controller;

import com.taxi.manager.controller.dto.CarDTO;
import com.taxi.manager.controller.dto.CreateDriverRequestDTO;
import com.taxi.manager.controller.dto.DriverDTO;
import com.taxi.manager.controller.dto.UpdateDriverRequestDTO;
import com.taxi.manager.model.Driver;
import com.taxi.manager.service.DriverService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Optional;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(DriverController.class)
public class DriverControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private DriverService driverService;

    @Test
    public void getById_EntryFound_ReturnHttpStatus200() throws Exception {
        Driver driver = Driver.builder()
                .id(1L)
                .name("Gleb")
                .city("Kiev")
                .languages("Eng")
                .drivingLicense("55834")
                .build();

        when(driverService.findById(1L)).thenReturn(Optional.of(driver));

        mockMvc.perform(get("/driver/{id}", driver.getId())
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.name", is("Gleb")))
                .andExpect(jsonPath("$.city", is("Kiev")))
                .andExpect(jsonPath("$.languages", is("Eng")))
                .andExpect(jsonPath("$.drivingLicense", is("55834")));
    }

    @Test
    public void getById_EntryNotFound_ReturnHttpStatus404() throws Exception {
        when(driverService.findById(1L)).thenReturn(Optional.empty());

        mockMvc.perform(get("/driver/{id}", 1))
                .andExpect(status().isNotFound());
    }


    @Test
    public void save_EntryValid_ReturnHttpStatus200() throws Exception {
        String json = "{\n" +
                "    \"name\": \"Gleb\",\n" +
                "    \"city\": \"Kiev\",\n" +
                "    \"languages\": \"Eng\",\n" +
                "    \"drivingLicense\": \"55834\",\n" +
                "    \"car\" : {\n" +
                "      \n" +
                "    }\n" +
                "}";

        CreateDriverRequestDTO createDriverRequestDTO = CreateDriverRequestDTO.builder()
                .name("Gleb")
                .city("Kiev")
                .languages("Eng")
                .drivingLicense("55834")
                .car(new CarDTO())
                .build();
        Driver driver = Driver.builder()
                .id(1L)
                .name("Gleb")
                .city("Kiev")
                .languages("Eng")
                .drivingLicense("55834")
                .build();

        when(driverService.save(eq(createDriverRequestDTO))).thenReturn(driver);

        mockMvc.perform(post("/driver")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(json))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.name", is("Gleb")))
                .andExpect(jsonPath("$.city", is("Kiev")))
                .andExpect(jsonPath("$.languages", is("Eng")))
                .andExpect(jsonPath("$.drivingLicense", is("55834")));
    }

    @Test
    public void save_EntryNotValid_ReturnHttpStatus400() throws Exception {
        String json = "{\n" +
                "    \"name\": null,\n" +
                "    \"city\": \"Kiev\",\n" +
                "    \"languages\": \"Eng\",\n" +
                "    \"drivingLicense\" : \"55834\",\n" +
                "    \"car\" : {\n" +
                "      \n" +
                "    }\n" +
                "}";

        mockMvc.perform(post("/driver")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(json))
                .andExpect(status().isBadRequest()).andDo(print());
    }

    @Test
    public void update_EntryValid_ReturnHttpStatus200() throws Exception {
        String json = "{\n" +
                "    \"name\": \"Gleb\",\n" +
                "    \"city\": \"Kiev\",\n" +
                "    \"languages\": \"Eng\",\n" +
                "    \"car\" : {\n" +
                "      \n" +
                "    }\n" +
                "}";

        UpdateDriverRequestDTO updateDriverRequestDTO = UpdateDriverRequestDTO.builder()
                .name("Gleb")
                .city("Kiev")
                .languages("Eng")
                .car(new CarDTO())
                .build();
        DriverDTO driverDTO = DriverDTO.builder()
                        .id(1L)
                        .name("Gleb")
                        .city("Kiev")
                        .languages("Eng")
                        .drivingLicense("55834")
                        .build();

        when(driverService.update(eq(updateDriverRequestDTO), eq(1L))).thenReturn(driverDTO);

        mockMvc.perform(put("/driver/{id}", 1)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(json))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.name", is("Gleb")))
                .andExpect(jsonPath("$.city", is("Kiev")))
                .andExpect(jsonPath("$.languages", is("Eng")))
                .andExpect(jsonPath("$.drivingLicense", is("55834")));
    }

    @Test
    public void update_EntryNotValid_ReturnHttpStatus400() throws Exception {
        String json = "{\n" +
                "    \"name\": \"Gleb\",\n" +
                "    \"city\": null,\n" +
                "    \"languages\": \"Eng\",\n" +
                "    \"car\" : {\n" +
                "      \n" +
                "    }\n" +
                "}";

        mockMvc.perform(put("/driver/{id}", 1)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(json))
                .andExpect(status().isBadRequest());
    }
}