package com.taxi.manager.controller;

import com.taxi.manager.controller.dto.CustomerRequestDTO;
import com.taxi.manager.model.Address;
import com.taxi.manager.model.Customer;
import com.taxi.manager.service.CustomerService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Optional;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest
public class CustomerControllerTest {

    @Autowired
    private CustomerController customerController;
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private CustomerService customerService;

    @Test
    public void findById_entityFound_status200() throws Exception {
        Customer customer = Customer.builder()
                .id(1L)
                .name("Gleb")
                .homeAddress(new Address("Kiev", "mazepy", "10"))
                .build();

        when(customerService.findById(1L)).thenReturn(Optional.of(customer));

        mockMvc.perform(get("/customer/{id}", customer.getId())
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.name", is("Gleb")))
                .andExpect(jsonPath("$.homeAddress.city", is("Kiev")))
                .andExpect(jsonPath("$.homeAddress.street", is("mazepy")))
                .andExpect(jsonPath("$.homeAddress.houseNumber", is("10")));
    }

    @Test
    public void findById_entityNotFound_status404() throws Exception {

        when(customerService.findById(1L)).thenReturn(Optional.empty());

        mockMvc.perform(get("/customer/{id}", 1)
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isNotFound());
    }

    @Test
    public void saveEntity_Valid_status200() throws Exception {
        String json = "{\n" +
                "\"name\": \"Gleb\",\n" +
                "\"homeAddress\": {\n" +
                "\"city\": \"Kiev\", \n" +
                "\"street\": \"mazepy\", \n" +
                "\"houseNumber\": \"10\" \n" +
                "  }\n" +
                "}";

        CustomerRequestDTO customerRequestDTO = CustomerRequestDTO.builder()
                .name("Gleb")
                .homeAddress(new Address("Kiev", "mazepy", "10"))
                .build();

        Customer customer = Customer.builder()
                .id(1L)
                .name("Gleb")
                .homeAddress(new Address("Kiev", "mazepy", "10"))
                .build();

        when(customerService.save(eq(customerRequestDTO))).thenReturn(customer);

        mockMvc.perform(post("/customer")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(json))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.name", is("Gleb")))
                .andExpect(jsonPath("$.homeAddress.city", is("Kiev")))
                .andExpect(jsonPath("$.homeAddress.street", is("mazepy")))
                .andExpect(jsonPath("$.homeAddress.houseNumber", is("10")));
    }

    @Test
    public void saveEntity_NotValid_status400() throws Exception {
        String json = "{\n" +
                "\"name\": null,\n" +
                "\"homeAddress\": {\n" +
                "\"city\": \"Kiev\", \n" +
                "\"street\": \"mazepy\", \n" +
                "\"houseNumber\": \"10\" \n" +
                "  }\n" +
                "}";

        mockMvc.perform(post("/customer")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(json))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void updateEntity_Valid_status200() throws Exception {
        String json = "{\n" +
                "\"name\": \"Gleb\",\n" +
                "\"homeAddress\": {\n" +
                "\"city\": \"Kiev\", \n" +
                "\"street\": \"mazepy\", \n" +
                "\"houseNumber\": \"10\" \n" +
                "  }\n" +
                "}";

        CustomerRequestDTO customerRequestDTO = CustomerRequestDTO.builder()
                .name("Gleb")
                .homeAddress(new Address("Kiev", "mazepy", "10"))
                .build();

        Customer customer = Customer.builder()
                .id(1L)
                .name("Gleb")
                .homeAddress(new Address("Kiev", "mazepy", "10"))
                .build();

        when(customerService.update(eq(customerRequestDTO), eq(1L))).thenReturn(customer);

        mockMvc.perform(put("/customer/{id}", customer.getId())
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(json))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.name", is("Gleb")))
                .andExpect(jsonPath("$.homeAddress.city", is("Kiev")))
                .andExpect(jsonPath("$.homeAddress.street", is("mazepy")))
                .andExpect(jsonPath("$.homeAddress.houseNumber", is("10")));
    }

    @Test
    public void updateEntity_NotValidFieldName_status400() throws Exception {
        String json = "{\n" +
                "\"name\": null,\n" +
                "\"homeAddress\": {\n" +
                "\"city\": \"Kiev\", \n" +
                "\"street\": mazepy, \n" +
                "\"houseNumber\": \"10\" \n" +
                "  }\n" +
                "}";

        mockMvc.perform(put("/customer/{id}", 1)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(json))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void updateEntity_NotValidFieldHomeAddress_status400() throws Exception {
        String json = "{\n" +
                "\"name\": \"Gleb\",\n" +
                "\"homeAddress\": {\n" +
                "\"city\": \"Kiev\", \n" +
                "\"street\": null, \n" +
                "\"houseNumber\": \"10\" \n" +
                "  }\n" +
                "}";

        mockMvc.perform(put("/customer/{id}", 1)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(json))
                .andExpect(status().isBadRequest());
    }
}