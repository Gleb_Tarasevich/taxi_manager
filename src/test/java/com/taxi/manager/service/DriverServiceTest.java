package com.taxi.manager.service;

import com.taxi.manager.controller.dto.CarDTO;
import com.taxi.manager.controller.dto.DriverDTO;
import com.taxi.manager.controller.dto.UpdateDriverRequestDTO;
import com.taxi.manager.exception.DriverNotFoundException;
import com.taxi.manager.model.Driver;
import com.taxi.manager.repository.DriverRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DriverServiceTest {

    @MockBean
    private DriverRepository driverRepository;
    @Autowired
    private DriverService driverService;

    @Test
    public void findById() {
        Driver driver = Driver.builder()
                .id(1L)
                .name("Gleb")
                .city("Kiev")
                .languages("Eng")
                .drivingLicense("55834")
                .build();

        when(driverRepository.findById(driver.getId())).thenReturn(Optional.of(driver));

        assertThat(driverService.findById(driver.getId())).isEqualTo(Optional.of(driver));
    }

    @Test
    public void findById_notFound() {
        Driver driver = Driver.builder()
                .id(1L)
                .name("Gleb")
                .city("Kiev")
                .languages("Eng")
                .drivingLicense("55834")
                .build();

        when(driverRepository.findById(driver.getId())).thenReturn(Optional.of(driver));

        assertThat(driverService.findById(2L)).isNotEqualTo(Optional.of(driver));
    }

    @Test
    public void updateEntry_findById_andUpdate() {
        Driver driver = Driver.builder()
                .id(1L)
                .name("Gleb")
                .city("Kiev")
                .languages("Eng")
                .build();

        UpdateDriverRequestDTO updateDriverRequestDTO = UpdateDriverRequestDTO.builder()
                .name("Gleb")
                .city("Vishgorod")
                .languages("Eng")
                .car(new CarDTO())
                .build();

        when(driverRepository.findById(1L)).thenReturn(Optional.of(driver));

        DriverDTO driverDTO = driverService.update(updateDriverRequestDTO, 1L);

        assertThat(driverDTO).isEqualTo(DriverDTO.from(driver));
    }

    @Test
    public void updateEntry_NotFindById() {
        Driver driver = Driver.builder()
                .id(1L)
                .name("Gleb")
                .city("Kiev")
                .languages("Eng")
                .build();

        UpdateDriverRequestDTO updateDriverRequestDTO = UpdateDriverRequestDTO.builder()
                .name("Gleb")
                .city("Vishgorod")
                .languages("Eng")
                .car(new CarDTO())
                .build();

        when(driverRepository.findById(1L)).thenReturn(Optional.of(driver));

        assertThatThrownBy(() -> driverService.update(updateDriverRequestDTO, 2L))
                .isInstanceOf(DriverNotFoundException.class);
    }
}