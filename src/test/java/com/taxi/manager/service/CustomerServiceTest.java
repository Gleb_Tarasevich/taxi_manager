package com.taxi.manager.service;

import com.taxi.manager.controller.dto.CustomerRequestDTO;
import com.taxi.manager.exception.CustomerNotFoundException;
import com.taxi.manager.model.Address;
import com.taxi.manager.model.Customer;
import com.taxi.manager.repository.CustomerRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomerServiceTest {

    @Autowired
    private CustomerService customerService;
    @MockBean
    private CustomerRepository customerRepository;

    @Test
    public void findById() {
        Customer customer = Customer.builder()
                .id(1L)
                .name("Gleb")
                .homeAddress(new Address("Kiev", "mazepy", "10"))
                .build();

        when(customerRepository.findById(1L)).thenReturn(Optional.of(customer));

        assertThat(customerService.findById(customer.getId())).isEqualTo(Optional.of(customer));
    }

    @Test
    public void NotFound() {
        when(customerRepository.findById(1L)).thenReturn(Optional.empty());

        Optional<Customer> customerTest = customerService.findById(1L);

        assertThat(customerTest).isEmpty();
    }

    @Test
    public void updateEntry() {
        CustomerRequestDTO customerRequestDTO = CustomerRequestDTO.builder()
                .name("Gleb")
                .homeAddress(new Address("Kiev", "mazepy", "11"))
                .build();

        Customer customer = Customer.builder()
                .id(1L)
                .name("Gleb")
                .homeAddress(new Address("Kiev", "mazepy", "10"))
                .build();

        when(customerRepository.findById(1L)).thenReturn(Optional.of(customer));

        Customer customerTest = customerService.update(customerRequestDTO, 1L);

        assertThat(customerTest).isEqualTo(customer);
    }

    @Test
    public void updateEntry_NotFoundEntry() {
        CustomerRequestDTO customerRequestDTO = CustomerRequestDTO.builder()
                .name("Gleb")
                .homeAddress(new Address("Kiev", "mazepy", "10"))
                .build();

        when(customerRepository.findById(1L)).thenReturn(Optional.empty());

        assertThatThrownBy(() -> customerService.update(customerRequestDTO, 1L))
                .isInstanceOf(CustomerNotFoundException.class);
    }
}