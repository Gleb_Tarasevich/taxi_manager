package com.taxi.manager.repository;

import com.taxi.manager.model.Car;
import com.taxi.manager.model.Driver;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.PersistenceException;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@RunWith(SpringRunner.class)
@DataJpaTest
public class DriverRepositoryTest {

    @Autowired
    private DriverRepository driverRepository;

    @Autowired
    private TestEntityManager entityManager;

    @Test
    public void save_and_findById() {
        Driver driver = Driver.builder()
                .name("Gleb")
                .city("Kiev")
                .languages("Eng")
                .drivingLicense("55834")
                .car(new Car())
                .build();

        driverRepository.save(driver);

        assertThat(driverRepository.findById(driver.getId())).isNotNull();
        assertThat(driverRepository.findById(driver.getId()).get().getName()).isNotNull();
        assertThat(driverRepository.findById(driver.getId()).get().getCity()).isNotNull();
        assertThat(driverRepository.findById(driver.getId()).get().getLanguages()).isNotNull();
        assertThat(driverRepository.findById(driver.getId()).get().getDrivingLicense()).isNotNull();
        assertThat(driverRepository.findById(driver.getId()).get().getCar()).isNotNull();
    }

    @Test
    public void save_and_notFindById() {
        Driver driver = Driver.builder()
                .name("Gleb")
                .city("Kiev")
                .languages("Eng")
                .drivingLicense("55834")
                .car(new Car())
                .build();

        driverRepository.save(driver);

        assertThat(driverRepository.findById(driver.getId() + 1)).isEqualTo(Optional.empty());
    }

    @Test
    public void saveNull() {
        Driver driver = Driver.builder()
                .name(null)
                .city(null)
                .languages(null)
                .drivingLicense(null)
                .car(null)
                .build();

        driverRepository.save(driver);

        assertThatThrownBy(() -> entityManager.flush()).isInstanceOf(PersistenceException.class);
    }
}