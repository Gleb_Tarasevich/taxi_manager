package com.taxi.manager.repository;

import com.taxi.manager.model.Address;
import com.taxi.manager.model.Customer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class CustomerRepositoryTest {

    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private TestEntityManager entityManager;

    @Test
    public void save_FindById() {
        Customer customer = Customer.builder()
                .name("Gleb")
                .homeAddress(new Address("Kiev", "mazepy", "10"))
                .build();

        customerRepository.save(customer);
        Optional<Customer> customerTest = customerRepository.findById(customer.getId());

        assertThat(customerTest).isEqualTo(Optional.of(customer));
        assertThat(customerTest.get().getName()).isNotEmpty();
        assertThat(customerTest.get().getHomeAddress()).isNotNull();
    }

    @Test
    public void save_NotFound() {
        Customer customer = Customer.builder()
                .name("Gleb")
                .homeAddress(new Address("Kiev", "mazepy", "10"))
                .build();

        customerRepository.save(customer);

        assertThat(customerRepository.findById(3L)).isEmpty();
    }
}